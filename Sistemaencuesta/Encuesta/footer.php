
<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
<div class="container">
<div class="mini.title">
	
	<center><h3>Nuestros servicios</h3></center>
</div>
	
		
	</div>

<footer class="footer-distributed">

			<div class="footer-left">

				

				<p class="footer-links">
					<a href="#">Inicio</a>
					·
					<a href="#">Encuesta</a>
					·
					<a href="#">Responder</a>
					·
					<a href="#">Login</a>
					·
					<a href="#">Contact</a>
				</p>

				<p class="footer-company-name">Universidad Peruana Unión &copy; 2016</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>Se parte de Nosotros</span> Perú, Lima</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>(01) 5186315, 6186316 - Anexo:2027</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="soporteupg@upeu.edu.pe">soporteupg@upeu.edu.pe</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>Unidad de Posgrado Salud</span>
					La unidad de Posgrado Salud realiza eventos como Jornadas Cientificas para poder desarrollar la capacidad de nuestros Professionales de Enfermería.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>

	</body>

</html>
