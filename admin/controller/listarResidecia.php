<?php

session_start();

//  if(!isset($_SESSION['cargo']) || $_SESSION['cargo'] != 1){
//    header('location: ../../index.php');
//  }

require_once '../model/personalist.php';

switch ($_GET["op"]) {
    case 'listar':
        $listar = new Lista();
        $listar->listar();
        break;
    case 'editar':
        $data = json_decode(file_get_contents("php://input"));
        $id = $data->persona_id;
        $nombre = $data->person_nombre;
        $tipocomp = $data->tipocomp;
        $razonsoc = $data->razonsoc;
        $ruc = $data->ruc;
        $direccion = $data->direccion;
        $person_fechin = $data->person_fechin;
        $person_fechout = $data->person_fechout;
        $asignado = $data->asignado;
        $comentario = $data->comentario;
        $comprobante = $data->comprobante;
        //echo $asignado;

        $dataResi = new Lista();
        $dataResi->update($id, $nombre, $tipocomp, $razonsoc, $ruc, $direccion ,$person_fechin, $person_fechout, $asignado, $comentario, $comprobante);
        break;

    case 'eliminar':
        $data = json_decode(file_get_contents("php://input"));
        $id = $data->persona_id;

        $dataResi = new Lista();
        $dataResi->eliminar($id);
        break;
    
    case 'totaolregistro':
        $totalinscritos = new Lista();
        $totalinscritos->totalinscritos();
        break;
    
    case 'totalconhabitacion':
        $totalinscritos = new Lista();
        $totalinscritos->totalconhabitacion();
        break;
    case 'totalsinhabitacion':
        $totalinscritos = new Lista();
        $totalinscritos->totalsinhabitacion();
        break;
}
?>