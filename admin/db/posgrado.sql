/*
SQLyog Community v8.81 
MySQL - 5.5.27 : Database - posgrado
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`magwi_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `magwi_db`;

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `persona_id` int(10) NOT NULL AUTO_INCREMENT,
  `person_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_unidad` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_lugar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_habit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_uso` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_fechin` date DEFAULT NULL,
  `person_horain` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_fechout` date DEFAULT NULL,
  `person_horaout` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_file` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persona_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `persona` */

insert  into `persona`(`persona_id`,`person_nombre`,`person_code`,`person_unidad`,`person_email`,`person_lugar`,`person_habit`,`person_uso`,`person_fechin`,`person_horain`,`person_fechout`,`person_horaout`,`person_file`) values (26,'William Calisaya Pari\r\n	    	','15088\r\n	    	','Posgrado de IngenierÃ­a y Arquitectur\r\n	    	','elmagwi@gmail.com\r\n	    	','tacna-peru\r\n	    	','Vip\r\n	    ','Entre dos\r','2017-05-04','111\r\n	    ','2017-05-10','11\r\n	    	','149582177810diazoracion.jpg\r\n    	'),(27,'marroni de la fuente\r\n	    	','123123\r\n	    	','Posgrado Ciencias Humanas y EducaciÃ³n\r\n	    	','elasdas\r\n	    	','2323\r\n	    	','Normal\r\n	 ','Entre dos\r','2017-05-04','11\r\n	    	','2017-05-19','222\r\n	    ','1495836384whatsapp-logo-PNG-Transparent.png\r\n    	'),(28,'jojojojjo\r\n	    	','123\r\n	    	','Posgrado Ciencias Empresariales\r\n	    	','asdasd\r\n	    	','asdas\r\n	    	','Vip\r\n	    ','Entre dos\r','2017-05-19','123\r\n	    ','2017-05-17','2342\r\n	   ','149583975110diazoracion.jpg\r\n    	');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `cargo` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`nombre`,`email`,`clave`,`cargo`) values (1,'michael yara','codigoadsi@gmail.com','202cb962ac59075b964b07152d234b70','1'),(2,'william','magwi','827ccb0eea8a706c4c34a16891f84e7b','2');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
