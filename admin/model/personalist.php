<?php

require_once('conexion.php');

class Lista extends Conexion {

    public function listar() {
        parent::conectar();

        $unidad = $_SESSION['unidad'];
        if ($unidad == "RESI" || $unidad == "ADMIN") {
            $consultalist = 'select * from registros where estado = 1';
        } else {
            $consultalist = 'select * from persona where person_unidad= "' . $unidad . '"';
        }
        $verificar_lista = parent::query($consultalist);

        $arr = array();

        if (mysqli_num_rows($verificar_lista) > 0) {

            while ($row = mysqli_fetch_assoc($verificar_lista)) {
                $arr[] = $row;
            }
        }
        echo json_encode($arr);
    }

    public function reporte() {
        parent::conectar();
        $reportesuni = 'SELECT person_unidad AS personas, COUNT(person_unidad)AS cantidad FROM persona GROUP BY person_unidad HAVING COUNT(person_unidad)';
        $verificar = parent::query($reportesuni);
        while ($resul = mysqli_fetch_array($verificar)) {
            echo '["' . $resul['personas'] . '" ,' . $resul["cantidad"] . '], ';
        }
    }

    public function update($id, $nombre, $tipocomp, $razonsoc, $ruc, $direccion, $person_fechin, $person_fechout, $asignado, $comentario, $comprobante) {
        parent::conectar();
        $updatepersona = 'UPDATE persona SET '
                . 'person_nombre = "' . $nombre . '", '
                . 'tipocomp="' . $tipocomp . '", '
                . 'razonsoc="' . $razonsoc . '",'
                . 'ruc="' . $ruc . '", '
                . 'direccion="' . $direccion . '" , '
                . 'person_fechin="' . $person_fechin . '", '
                . 'person_fechout="' . $person_fechout . '", asignado="' . $asignado . '", comentario="' . $comentario . '", comprobante="'.$comprobante.'" WHERE persona_id = ' . $id . '';
        $actualizado = parent::query($updatepersona);
        //if (mysqli_num_rows($actualizado) > 0) {
        $respuesta = 'Se actualizó correctamente!';
        echo $respuesta;
        //}
    }

    public function eliminar($id) {
        parent::conectar();
        $updatepersona = 'DELETE FROM persona WHERE persona_id = ' . $id . '';
        $actualizado = parent::query($updatepersona);
        //if (mysqli_num_rows($actualizado) > 0) {
        $respuesta = 'eliminado';
        echo $respuesta;
        //}
    }

    public function totalinscritos() {
        parent::conectar();
      
        $unidad = $_SESSION['unidad'];
        if ($unidad != "RESI" && $unidad != "ADMIN") {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1 and person_unidad= "' . $unidad . '"';
        } else {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1';
        }

        $totresult = parent::consultaArreglo($consultalist);
        $tot = $totresult['total'];
        echo $tot;
    }

    public function totalconhabitacion() {
        parent::conectar();

        $unidad = $_SESSION['unidad'];
        if ($unidad != "RESI" && $unidad != "ADMIN") {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1 and person_unidad= "' . $unidad . '" and asignado="SI" ';
        } else {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1 and asignado="SI"';
        }
        $totresult = parent::consultaArreglo($consultalist);
        $tot = $totresult['total'];
        echo $tot;
    }
    
    public function totalsinhabitacion() {
        parent::conectar();

        $unidad = $_SESSION['unidad'];
        if ($unidad != "RESI" && $unidad != "ADMIN") {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1 and person_unidad= "' . $unidad . '" and asignado <> "SI" ';
        } else {
            $consultalist = 'SELECT COUNT(*) AS total FROM persona WHERE tipevent= 1 and asignado <> "SI"';
        }
        $totresult = parent::consultaArreglo($consultalist);
        $tot = $totresult['total'];
        echo $tot;
    }

}

;
?>
