// Application module
var crudApp = angular.module('crudApp', []);
crudApp.controller("DbControllerRes", ['$scope', '$http', function ($scope, $http, push) {

// Function to get employee details from the database


        /*$scope.listatot = function() {
         // Sending request to EmpDetails.php files 
         $http.post('../../controller/listtot.php').then(function(tot) {
         console.log("llegando q mas: ", tot)
         //$scope.totreg = tot;
         $scope.totreg = {};
         
         angular.forEach(tot.data, function(result){
         $scope.totreg.push(result)
         //$scope.xyz = resul.total;
         });
         
         
         console.log('aqui va: ', $scope.totreg);
         
         });
         };*/
        //$scope.listatot();
        

        $scope.listarpersonas = function () {
            // Sending request to EmpDetails.php files 
            $http.get('../../controller/listarResidecia.php?op=listar').then(function (data) {
                console.log("llegando: ", data)
                $scope.details = data.data;

            });
        };
        
        $scope.totalinscritos = function (){
            $http.get('../../controller/listarResidecia.php?op=totaolregistro').then(function (totalregistro) {
                console.log("llego el total: ", totalregistro.data)
                $scope.totalregistro = totalregistro.data;
            });
        }

        $scope.totaconhabitacion = function (){
            $http.get('../../controller/listarResidecia.php?op=totalconhabitacion').then(function (totalregistro) {
                console.log("llego el total con habitacion: ", totalregistro.data)
                $scope.totalconhabitacion = totalregistro.data;
            });
        }
        
        $scope.totasinhabitacion = function (){
            $http.get('../../controller/listarResidecia.php?op=totalsinhabitacion').then(function (totalregistro) {
                console.log("llego el total sin habitacion: ", totalregistro.data)
                $scope.totalsinhabitacion = totalregistro.data;
            });
        }
        

        $scope.editpersona = function (info) {
            console.log('-->',info);
            $scope.personadetail = info;
        };
        
        $scope.updatepage = function () {
            $scope.listarpersonas();
            $scope.totalinscritos();
            $scope.totaconhabitacion();
            $scope.totasinhabitacion();
        };

        $scope.updatepersona = function (datopersona) {
            $http.post('../../controller/listarResidecia.php?op=editar',
                    {
                        "persona_id": datopersona.persona_id,
                        "person_nombre": datopersona.person_nombre,
                        "tipocomp": datopersona.tipocomp,
                        "razonsoc": datopersona.razonsoc,
                        "ruc": datopersona.ruc,
                        "direccion": datopersona.direccion,
                        "person_fechin": datopersona.person_fechin,
                        "person_fechout": datopersona.person_fechout,
                        "asignado": datopersona.asignado,
                        "comentario": datopersona.comentario,
                        "comprobante": datopersona.comprobante
                    }).then(function (ok) {
                $scope.enviado = ok.data;
                swal('', $scope.enviado, 'success');
//                console.log("ARRAY: ", algo);
                $scope.updatepage();
                console.log("Actualizado ok");
                $('#editForm').click();
            });
        }
        
        $scope.del = function(id){
            $http.post('../../controller/listarResidecia.php?op=eliminar',
                        {
                            "persona_id": id
                        }).then(function (ok) {
                    $scope.eliminado = ok.data;

//                console.log("ARRAY: ", algo);
                    $scope.updatepage();
                    console.log("Se elimini ok: ", $scope.eliminado);
                    $('#editForm').click();
                    //swal('', $scope.eliminado, 'success');
                });
        }

        $scope.deletepersona = function (id) {
            swal({
                title: "Desea eliminar?",
                text: "Una vez eliminado, ya no se podrá recuperar !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar!",
                closeOnConfirm: false
            },
                    function () {
                        $scope.del(id);
                        swal("Eliminado!", "El registro fue eliminado.", "success");
                    });
                    
            
        };
        $scope.updatepage();





// Setting default value of gender 
        //$scope.empInfo = {'gender': 'male'};
// Enabling show_form variable to enable Add employee button
        //$scope.show_form = true;
// Function to add toggle behaviour to form
        $scope.formToggle = function () {
            $('#empForm').slideToggle();
            $('#editForm').css('display', 'none');
        }


        $scope.updateMsg = function (emp_id) {
            console.log("esto es el mensaja de vuelta: ", emp_id);
            $('#editForm').css('display', 'none');
        }
    }]);

crudApp.controller("DbControllerPsic", ['$scope', '$http', function ($scope, $http) {
        $scope.listarpsico = function () {
            $http.get('../../controller/listarPsico.php').then(function (data) {
                console.log("llegando: ", data)
                $scope.details = data.data;
            });
        };
        $scope.listarpsico();
        $scope.formToggle = function () {
            $('#empForm').slideToggle();
            $('#editForm').css('display', 'none');
        }
        $scope.updateMsg = function (emp_id) {
            console.log("esto es el mensaja de vuelta: ", emp_id);
            $('#editForm').css('display', 'none');
        }
        //------------------------->>>
        //$scope.encontrado = [];
        $scope.retorname = function (recibido) {
            console.log("aqui -> ", recibido.persona_id);
            $http.post('../../controller/retorname.php',
                    {
                        "persona_id": recibido.persona_id,
                        "person_nombre": recibido.person_nombre,
                        "person_email": recibido.person_email
                    }).then(function (algo) {

                console.log("Haber que retorno: ", algo);
                $scope.enviado = algo.data;
                swal('Enviado', $scope.enviado);
//                console.log("ARRAY: ", algo);
                $scope.listarpsico();
            });



        };
    }]);