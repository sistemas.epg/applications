<?php
// Se prendio esta mrd :v
session_start();

// Validamos que exista una session y ademas que el cargo que exista sea igual a 1 (Administrador)
if (!isset($_SESSION['cargo']) || $_SESSION['cargo'] != 1) {
    /*
      Para redireccionar en php se utiliza header,
      pero al ser datos enviados por cabereza debe ejecutarse
      antes de mostrar cualquier informacion en el DOM es por eso que inserto este
      codigo antes de la estructura del html, espero haber sido claro
     */
    header('location: ../../index.php');
}
?>
<!DOCTYPE html>
<html lang="es" ng-app="crudApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>REGISTRO CURSO TALLER</title>
        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/metisMenu.min.css" rel="stylesheet">
        <link href="../../css/timeline.css" rel="stylesheet">
        <link href="../../css/startmin.css" rel="stylesheet">
        <link href="../../css/morris.css" rel="stylesheet">
        <link rel="stylesheet" href="../../css/sweetalert.css">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- <link rel="stylesheet" href="../../../admision/public/css/bootstrap-datetimepicker.min.css"> -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/sweetalert.min.js"></script>
        <script src="../../js/angular.min.js"></script>
    </head>

    <body ng-controller="DbControllerRes">

        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><i class="fa fa-home fa-fw"></i>REGISTRO CURSO TALLER</a>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="navbar" class="navbar-collapse collapse">
                    <!--                    <ul class="nav navbar-nav">
                                            <li><a href="registrados.php"><i class="fa fa-table fa-fw"></i>Inscritos</a></li>
                                            <li><a href="estadisticaresidencia.php"><i class="fa fa-address-book fa-fw"></i>Reporte</a></li>
                                        </ul>-->
                    <!-- Top Navigation: Right Menu -->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="registrados.php">Inscritos <span class="glyphicon glyphicon-list-alt"></span></a></a></li>
                        <li><a href="estadisticaresidencia.php">Reporte <span class="glyphicon glyphicon-stats"></span></a></li>


                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user fa-fw"></i> <?php echo ucfirst($_SESSION['nombre']); ?> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li class="divider"></li>
                                <li><a href="../../controller/cerrarSesion.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <!-- Sidebar -->
                <!--                        <div class="navbar-default sidebar" role="navigation">
                                            <div class="sidebar-nav navbar-collapse">
                                
                                                <ul class="nav" id="side-menu">
                                                    <li class="sidebar-search">
                                                        <div class="input-group custom-search-form">
                                                            <input type="text" class="form-control" placeholder="Search...">
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-primary" type="button">
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="registrados.php"><i class="fa fa-table fa-fw"></i>Inscritos residecias</a>
                                                    </li>
                                                </ul>
                                
                                            </div>
                                        </div>-->
            </nav>

            <!-- Page Content -->
            <!--    <div id="page-wrapper">-->
            <div id="">
                <div class="container-fluid">
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Panel de registro</h1>
                        </div>
                    </div>

                    <!-- row -->
                    <div class="row">
                        <!-- <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-address-book fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{totalregistro}}</div>
                                            <div>Total usuarios registrados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-home fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{totalconhabitacion}}</div>
                                            <div>Usuarios con habitación</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-ban fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{totalsinhabitacion}}</div>
                                            <div>Usuarios sin habitación</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-3 col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>Actualizar todos los datos</div>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="panel-footer">
                                        <span class="pull-right">
                                            <button class="btn btn-primary" style="right:inherit; " ng-click="updatepage()" title="Actualizar la pagina">
                                    <span class="glyphicon glyphicon-refresh"></span> Actualizar
                                            </button>
                                                
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                
                            </div>
                        </div> -->
                    </div>
                    <!-- /.row -->
                    
                    <!-- ... Your content goes here ... -->

                    <nav class="navbar navbar-default">
                        <div class="navbar-header">

                            <div class="alert alert-default input-group search-box">
                                <span class="input-group-btn">
                                    <input type="text" class="form-control" placeholder="Buscar por Nombres ó Apellidos" ng-model="search_query">
                                </span>
                            </div>

                        </div>
                    </nav>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Persona</th>
                                <th>Email</th>
                                <th>Celular</th>
                                
                                <th>Curso Taller</th>
                                <th>Egresado UPeU</th>
                                <th>Fecha Registro</th>
                                <th>Comentario</th>
                                <th>Boucher</th>
                                <!-- <th>Opciones</th> -->
                            </tr>

                            <tr ng-repeat="detail in details| filter:search_query">
                                <td>{{$index + 1}}</td>
                                <td>{{detail.nombres}}</td>
                                <td>{{detail.email}}</td>
                                <td>{{detail.celular}}</td>
                                
                                <td>{{detail.talleres_id}}</td>
                                <td>{{detail.egresadoupeu}}</td>
                                <td>{{detail.fechareg | date:'dd/MMMM/yy'}}</td>
                                <td ng-if="detail.comentario == ''"></td>
                                <td ng-if="detail.comentario != ''"><h4><span class="glyphicon glyphicon-envelope" title="{{detail.comentario}}" ></span></h4></td>
                                <td ng-if="detail.boucher == ''"></td>
                                <!-- <td ng-if="detail.boucher != ''"><a target="../" href="http://localhost:8000{{detail.boucher}}" title="ver boucher" class="btn btn-success" ><span class="glyphicon glyphicon-folder-open"></span></a></td> -->
                                <td ng-if="detail.boucher != ''"><a target="../" href="http://posgrado.upeu.edu.pe{{detail.boucher}}" title="ver boucher" class="btn btn-success" ><span class="glyphicon glyphicon-folder-open"></span></a></td>
                                <!-- <td>
                                    <button class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-lg" ng-click="editpersona(detail)" title="Ver informacion completa">
                                        <span class="glyphicon glyphicon-th-list"></span>
                                    </button>
                                </td> -->

                            </tr>
                        </table>
                    </div>

                    <!-- modal -->
                    <!--                    <button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>-->
                    <div class="modal fade bd-example-modal-lg" id="" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" id="editForm" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Detalles Persona</h4>
                                    {{enviado}}
                                </div>
                                <div class="modal-body">
                                    <div class="">
                                        <form class=""  ng-submit="">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="Name">Nombres:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_nombre" value="{{personadetail.person_nombre}}">
                                                    <input type="hidden" class="form-control" ng-model="personadetail.persona_id" value="{{personadetail.persona_id}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Codigo:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_code" value="{{personadetail.person_code}}" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="Name">Unidad:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_unidad" value="{{personadetail.person_unidad}}" disabled>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Email:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_email" value="{{personadetail.person_email}}" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="Name">Lugar de procedencia:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_lugar" value="{{personadetail.person_lugar}}" disabled>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Tipo de servicio:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_habit" value="{{personadetail.person_habit}}" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="Name">Fecha ingreso:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_fechin" id="person_fechin" value="{{personadetail.person_fechin}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Hora ingreso:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_horain" value="{{personadetail.person_horain}}" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="Name">Fecha Salida:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_fechout" id="person_fechout" value="{{personadetail.person_fechout}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Hora Salida:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.person_horaout" value="{{personadetail.person_horaout}}" disabled>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="Name">Tipo Comprobante :</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.tipocomp" id="person_fechout" value="{{personadetail.tipocomp}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="Email">Razon Social:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.razonsoc" value="{{personadetail.razonsoc}}">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="Name">Ruc :</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.ruc" id="person_fechout" value="{{personadetail.ruc}}">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Email">Direccion:</label>
                                                    <input type="text" class="form-control" ng-model="personadetail.direccion" value="{{personadetail.direccion}}">
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Comentarios">Comentarios:</label>
    <!--                                                <input type="textarea" class="form-control" ng-model="personadetail.comentario" value="{{personadetail.comentario}}">-->
                                                    <textarea class="form-control" rows="3" ng-model="personadetail.comentario" value="{{personadetail.comentario}}"></textarea>
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-md-4">
                                                <?php
                                                    $unidad = $_SESSION['unidad'];
                                                    if ($unidad == "RESI" || $unidad == "ADMIN") {
                                                        echo '<div class="">
                                                        <button type="button" class="btn btn-default"  ng-click="deletepersona(personadetail.persona_id)" >Eliminar Registro <span class="glyphicon glyphicon-warning-sign"></span></button>
                                                    </div>';
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?>
                                                    
                                                </div>
                                                <div class="col-md-4 alert alert-success">
                                                    <label for="Email">Comprobante generada:</label>
                                                    <input 
                                                    <?php
                                                    $unidad = $_SESSION['unidad'];
                                                    if ($unidad == "RESI" || $unidad == "ADMIN") {
                                                        echo 'type="checkbox"';
                                                    } else {
                                                        echo 'type="hidden"';
                                                    }
                                                    ?>
                                                        ng-model="personadetail.comprobante"
                                                        ng-true-value="'SI'" ng-false-value="'NO'"> {{personadetail.comprobante}}
                                                        <span class="glyphicon glyphicon-usd"></span>

                                                </div>
                                                <div class="col-md-4 alert alert-info">



                                                    <label for="Email">Habitacion asignada:</label>

                                                    <input 
                                                    <?php
                                                    $unidad = $_SESSION['unidad'];
                                                    if ($unidad == "RESI" || $unidad == "ADMIN") {
                                                        echo 'type="checkbox"';
                                                    } else {
                                                        echo 'type="hidden"';
                                                    }
                                                    ?>
                                                        ng-model="personadetail.asignado"
                                                        ng-true-value="'SI'" ng-false-value="'NO'"> {{personadetail.asignado}}
                                                        <span class="glyphicon glyphicon-home"></span>
                                                </div>

                                            </div>

<!--<tt>value2 = {{personadetail.asignado}}</tt><br/>-->
                                            <!-- <div class="form-group">
                                                <button type="button" class="btn btn-warning" ng-disabled="" ng-click="cerrar(personadetail)" >Update</button>
                                            </div> -->
                                        </form>

                                    </div>


                                </div>
                                <div class="modal-footer">




                                    <button type="button" class="btn btn-success" ng-click="updatepersona(personadetail)" >Guardar <span class="glyphicon glyphicon-floppy-disk"></span></button>
                                    <button type="button"  class="btn btn-danger" data-dismiss="modal">Cerrar <span class="glyphicon glyphicon-remove"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->

                </div>
            </div>

        </div>

        <!-- jQuery -->
        <script src="../js/personas.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/metisMenu.min.js"></script>
        <script src="../../js/startmin.js"></script>
        <script src="../../../admision/public/js/bootstrap-datetimepicker.min.js"></script>
        <script src="../../../admision/public/js/bootstrap-datetimepicker.es.js"></script>

    </body>

    <script>

                                        $(function () {

                                            $('#person_fechin').datetimepicker(
                                                    {
                                                        format: "yyyy-mm-dd",
                                                        language: 'es',
                                                        weekStart: 1,
                                                        todayBtn: 1,
                                                        autoclose: 1,
                                                        todayHighlight: 1,
                                                        startView: 2,
                                                        minView: 2,
                                                        forceParse: 0
                                                    }
                                            );
                                            $('#person_fechout').datetimepicker(
                                                    {
                                                        format: "yyyy-mm-dd",
                                                        language: 'es',
                                                        weekStart: 1,
                                                        todayBtn: 1,
                                                        autoclose: 1,
                                                        todayHighlight: 1,
                                                        startView: 2,
                                                        minView: 2,
                                                        forceParse: 0
                                                    }
                                            );
                                        });
    </script>
</html>