$(document).ready(function() {
	
	$('.button').click(function() {
		
		type = $(this).attr('data-type');
		
		$('.overlay-container-popup').fadeIn(function() {
			
			window.setTimeout(function(){
				$('.window-container-popup.'+type).addClass('window-container-visible-popup');
			}, 100);
			
		});
	});
	
	$('.close-popup').click(function() {
		$('.overlay-container-popup').fadeOut().end().find('.window-container-popup').removeClass('window-container-visible-popup');
	});
	
});